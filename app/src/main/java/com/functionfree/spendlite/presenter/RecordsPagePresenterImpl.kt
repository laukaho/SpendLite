package com.functionfree.spendlite.presenter

import android.util.Log
import com.functionfree.spendlite.model.*
import com.functionfree.spendlite.view.RecordsPageView

class RecordsPagePresenterImpl(recordsPageView: RecordsPageView) : RecordsPagePresenter{

    private val recordsPageView: RecordsPageView = recordsPageView
    var wallet:Wallet = Wallet("HKD", "First")

    init{

        recordsPageView.updateBalance(this.wallet.balance)
        recordsPageView.updateExpense(this.wallet.expenses)
        recordsPageView.updateIncome(this.wallet.expenses)
        recordsPageView.showAllRecords(this.wallet.getRecords("",""))
    }

    override fun addRecord(cost: Float, date: String, cat: Category, memo: String) {
        val nRecord:Record
        if(cost > 0)
        {
            nRecord = Income(cost, date, cat, memo)
            addIncome(nRecord)
        }else{
            nRecord = Expense(cost, date, cat, memo)
            addExpense(nRecord)
        }
        recordsPageView.showRecord(nRecord)
    }

    private fun addIncome(record:Income){
        this.wallet.addRecord(record)
        recordsPageView.updateIncome(this.wallet.income)
        recordsPageView.updateBalance(this.wallet.balance)
    }

    private fun addExpense(record:Expense){
        this.wallet.addRecord(record)
        recordsPageView.updateExpense(this.wallet.expenses)
        recordsPageView.updateBalance(this.wallet.balance)
    }

    override fun deleteRecord(record: Record) {
        this.wallet.deleteRecord(record)
        recordsPageView.removeRecord(record)
    }

}