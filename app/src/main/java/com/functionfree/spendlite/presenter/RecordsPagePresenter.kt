package com.functionfree.spendlite.presenter

import com.functionfree.spendlite.model.Category
import com.functionfree.spendlite.model.Record
import com.functionfree.spendlite.model.Wallet

public interface RecordsPagePresenter{

    fun addRecord(cost: Float, date: String, cat: Category, memo:String = "")
    fun deleteRecord(record: Record)
}