package com.functionfree.spendlite

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.functionfree.spendlite.model.Category
import com.functionfree.spendlite.model.InCategory
import com.functionfree.spendlite.model.Record
import com.functionfree.spendlite.presenter.RecordsPagePresenterImpl
import com.functionfree.spendlite.view.RecordsPageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), RecordsPageView {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recordsPagePresenter = RecordsPagePresenterImpl(this)
        btn_in.setOnClickListener(){
            Toast.makeText(this, "OnClick", Toast.LENGTH_SHORT).show()
            recordsPagePresenter.addRecord(123f, "1231-1231", InCategory("1231"), "")
        }

        btn_out.setOnClickListener(){
            Toast.makeText(this, "OnClick", Toast.LENGTH_SHORT).show()
            recordsPagePresenter.addRecord(-123f, "1231-1231", InCategory("1231"), "")
        }

    }

    override fun showAllRecords(records: List<Record>) {
       Log.d("MainActivity", "showAllRecords")
    }

    override fun updateIncome(value: Float) {
        tv_income.text = value.toString()
    }

    override fun updateExpense(value: Float) {
        tv_expense.text = value.toString()
    }

    override fun updateBalance(value: Float) {
        tv_balance.text = value.toString()
    }

    override fun showRecord(record: Record) {
        Log.d("MainActivity", "showRecord")
    }

    override fun removeRecord(record: Record) {
        Log.d("MainActivity", "removeRecord")
    }

}
