package com.functionfree.spendlite.model

abstract class Category(name:String){
    val name = name
    val icon:String = "plcaeholder"
}