package com.functionfree.spendlite.model

class Wallet(currency: String, name: String, records: MutableList<Record> =  mutableListOf<Record>()){
     var balance:Float = 0f
         private set
         get() = field

     var income:Float = 0f
         private set
         get() = field

     var expenses:Float = 0f
         private set
         get() = field

    val name:String = name

    private val records:MutableList<Record> = records

    init{
        for (record in records){
            this.balance += record.cost
            this.income = if(record is Income) this.income + record.cost else this.income
            this.expenses = if(record is Income) this.expenses + record.cost else this.expenses
        }
    }

    public fun addRecord(income:Income){
        this.balance += income.cost
        this.income += income.cost
        this.records.add(income)
    }

    public fun addRecord(expense:Expense){
        this.balance -= expense.cost
        this.expenses += expense.cost
        this.records.add(expense)
    }

    public fun deleteRecord(record:Record){
        this.balance -= record.cost
        this.records.remove(record)
    }

    public fun getRecords(startDate:String, endDate:String):List<Record>{
        return this.records
    }

}

