package com.functionfree.spendlite.view

import com.functionfree.spendlite.model.Category
import com.functionfree.spendlite.model.Record

public interface RecordsPageView{

    fun showAllRecords(records:List<Record>)
    fun updateIncome(value:Float)
    fun updateExpense(value:Float)
    fun updateBalance(value:Float)
    fun showRecord(record:Record)
    fun removeRecord(record:Record)
}