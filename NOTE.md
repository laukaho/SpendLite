# Kotlin Developement Issue

*2019-01-19*
#### Fragment crash

Fragment Transaction can only commit once
```
    //Get FragmentTransaction for every commit
    val fragmentTransaction = fragmentManager.beginTransaction()
    ...
    fragmentTransaction.commit()
```

*2019-01-19*

#### Fragment not showing
Use replace instead of add if fragment is not shown onCreate()
```
    btn_op_frag.setOnClickListener(){
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frag_container, fragment)
        fragmentTransaction.commit()
    }
```

#

*2019-01-18*
#### Unresolve R Issue

Change build gradle
```
    dependencies {
        classpath 'com.android.tools.build:gradle:3.3.0'
    }
```
to
```
    dependencies {
        classpath 'com.android.tools.build:gradle:3.2.1'
    }
```

*2019-01-18*
#### Use of RecyclerView 

```
var viewManager = LinearLayoutManager(this)
//Put the data to adapter
var viewAdapter = RecyclerViewAdpater(records)

var recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    setHasFixedSize(true)
    //Assign layout manager
    layoutManager = viewManager
    //Assign recycler view adapter
    adapter = viewAdapter

}
```

Implmentation of adapter class
```
class RecyclerViewAdpater(private val myDataset: Array<Record>) :
        RecyclerView.Adapter<RecyclerViewAdpater.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerViewAdpater.MyViewHolder {
        // create a new view

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.layout_record, parent, false)
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(itemView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.textView.text = myDataset[position]
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}
```

reference: https://github.com/aws1994/rv2gridlayout