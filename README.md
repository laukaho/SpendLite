# SpendLite
Capital Management android app by Kotlin

## App Features
- Record expense and income
- View records with filtering
  - Time range
  - Category
  - Memo
- Synchonization
  - Facebook
  - Google

- Report generation
  - pdf to email

# License

This project is licensed under the terms of the MIT license
